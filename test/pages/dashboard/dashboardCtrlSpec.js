describe('dashboardCtrl', function() {
  
	beforeEach(module('app'), function($httpBackend){
		 $httpBackend.whenGET('config.json').respond(readJSON('test/config.json'));
	});

  var $controller;
  var $compile;
  var formHelperService;
  var exchangeRateService;

  beforeEach(inject(function(_$controller_,_$compile_,_formHelperService_,_exchangeRateService_){
	  $controller = _$controller_;
	  $compile = _$compile_;
	  formHelperService = _formHelperService_;
	  exchangeRateService = _exchangeRateService_;	  
  }));
  
  
  it('should exist dependencies', function() {
	  expect($controller).toBeDefined();
	  expect($compile).toBeDefined();
	  expect(formHelperService).toBeDefined();
	  expect(exchangeRateService).toBeDefined();
  });
  
  beforeEach(() => {
	  spyOn(exchangeRateService, 'lastest')
	    .and.returnValue({
	    	base:"USD",
	    	date:"2018-02-22",
	    	rates:{
	    		EUR:0.8146
	    	}
	    });
	});
  
//  it('Exchange rate service is a mock', function() {
//	  expect(exchangeRateService.lastest().then).toBeDefined();
//	  expect($compile).toBeDefined();
//	  expect(formHelperService).toBeDefined();
//	  expect(exchangeRateService).toBeDefined();
//  });
//
//  describe('$scope.grade', function() {
//    it('sets the strength to "strong" if the password length is >8 chars', function() {
//      var $scope = {};
//      var controller = $controller('dashboardCtrl', { $scope: $scope });
//      $scope.password = 'longerthaneightchars';
//      $scope.grade();
//      expect($scope.strength).toEqual('strong');
//    });
//  });
});