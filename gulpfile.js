var gulp = require("gulp");
var path = require('path');
var template = require('gulp-template');
var rename = require('gulp-rename');
var batch = require('gulp-batch');
var toCase = require('to-case');
var sourcemaps = require('gulp-sourcemaps');
var babel = require("gulp-babel");
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cssMin = require('gulp-css');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var ngAnnotate = require('gulp-ng-annotate');
var minifyHTML = require('gulp-minify-html');
var server = require('gulp-server-livereload');
var jsonMinify = require('gulp-json-minify');
var clean = require('gulp-clean');
var imagemin = require('gulp-imagemin');
var jshint = require('gulp-jshint');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
var strip = require('gulp-strip-comments');
var fs = require('fs');
var merge = require('merge-stream');
var prop = require('./gulp.json');

var argv = require('yargs')
.usage('Usage: gulp <command> [options]')
.command(['component','page','service','provider','filter','directive'], 'Create component', {
	name: {
		describe: 'Component name',
		demandOption: true,
		requiresArg: true,
        type: 'string',
	}
})
.command('core', 'Minify javascript, html, sass, json files from app folder and send to dest path.')
.command('scripts', 'Minify javascript dependencies (Section libs in gulp.json) and app Js files and merge in app.js file into dest path.')
.command('styles', 'Minify css dependencies (Section styles in gulp.json) and app sass files and merge in app.css file into dest path.')
.command('fonts', 'Move fonts dependencies (Section fonts in gulp.json) to dest path.')
.command('watch', 'Check app files changes and run core task.')
.command('clean', 'Delete dest folder.')
.command('imagemin', 'Minify images files and move to dest path.')
.command('serve', 'Run our app, check app file changes and livereload.')
.command('template', 'Minify and move HTML and XML files to destination folder.')
.command('json', 'Minify and move json files to destination folder.')
.command('temp', 'Move files from temp folder to destination folder.')
.command('build', 'Run clean, scripts, styles, fonts and imagemin task.')
.help()
.argv;

var autoprefixerOptions = {
	browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

// Create component
gulp.task('component',function(){
	var name = toCase.slug(argv.name);
	gulp.src('./templates/component/*.**')
		.pipe(template({
			name: name,
			camelCaseName: toCase.camel(name)
		}))
		.pipe(rename(function(path){
			path.basename = path.basename.replace('temp', name);
		}))
		.pipe(gulp.dest(path.join(prop.config.path+'/components/', name)));
	
});


// Create page
gulp.task('page',function(){
	var name = toCase.slug(argv.name);
	var camelCaseName = toCase.camel(name);
	gulp.src('./templates/page/*.**')
		.pipe(template({
			name: name,
			camelCaseName: camelCaseName
		}))
		.pipe(rename(function(path){
			if(path.extname === '.js'){
				path.basename = path.basename.replace('temp', camelCaseName);				
			}else{
				path.basename = path.basename.replace('temp', name);
			}
		}))
		.pipe(gulp.dest(path.join(prop.config.path+'/pages/', name)));

});

//Create service
gulp.task('service',function(){
	var name = toCase.slug(argv.name);
	var camelCaseName = toCase.camel(name);
	
	gulp.src('./templates/service/tempService.js')
		.pipe(template({
			name: name,
			camelCaseName: camelCaseName
		}))
		.pipe(rename(function(path){
			path.basename = path.basename.replace('temp', camelCaseName);
		}))
		.pipe(gulp.dest(path.join(prop.config.path+'/services/')));

});

//Create directive
gulp.task('directive',function(){
	var name = toCase.slug(prop.config.prefix+'-'+argv.name);
	var camelCaseName = toCase.camel(name);
	
	gulp.src('./templates/directive/tempDirective.js')
		.pipe(template({
			name: name,
			camelCaseName: camelCaseName
		}))
		.pipe(rename(function(path){
			path.basename = path.basename.replace('temp', camelCaseName);
		}))
		.pipe(gulp.dest(path.join(prop.config.path+'/directives/')));
});

//Create filter
gulp.task('filter',function(){
	var name = toCase.slug(argv.name);
	var camelCaseName = toCase.camel(name);
	
	gulp.src('./templates/filter/tempFilter.js')
		.pipe(template({
			name: name,
			camelCaseName: camelCaseName
		}))
		.pipe(rename(function(path){
			path.basename = path.basename.replace('temp', camelCaseName);
		}))
		.pipe(gulp.dest(path.join(prop.config.path+'/filters/')));
});

//Create provider
gulp.task('provider',function(){
	var name = toCase.slug(argv.name);
	var camelCaseName = toCase.camel(name);
	
	gulp.src('./templates/service/tempProvider.js')
		.pipe(template({
			name: name,
			camelCaseName: camelCaseName
		}))
		.pipe(rename(function(path){
			path.basename = path.basename.replace('temp', camelCaseName);
		}))
		.pipe(gulp.dest(path.join(prop.config.path+'/services/')));

});

//Minify core and libs JS files and  copy result to destination path 
gulp.task('scripts',function(){
	var env = argv.env;
	var appStream = gulp.src(prop.libs.concat(['!'+prop.config.path+'/_temp/**',prop.config.path+'/**/*Module.js',prop.config.path+'/**/*.js']))
						.pipe(jshint())
						.pipe(gulpif(env == 'dev',sourcemaps.init()))
						.pipe(concat('app.js'))
					    //.pipe(babel({presets: ['es2015']}))
					    .pipe(ngAnnotate())
					    .pipe(uglify().on('error', function(e){console.log(e);}))
					    .pipe(gulpif(env == 'dev',sourcemaps.write('./maps')))
					    .pipe(gulp.dest(prop.config.dest+'/assets/js'));
});

//Minify core and libs CSS and SASS files and  copy result to destination path
gulp.task('styles',function(){
	appStream = gulp.src(['!'+prop.config.path+'/_temp/**',prop.config.path+'/**/*.scss'])
		.pipe(concat('app.css'))		
		.pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
		.pipe(postcss([ autoprefixer(autoprefixerOptions) ]));
	
	vendorStream = gulp.src(prop.styles)
		.pipe(concat('vendor.css'))
		.pipe(strip.text())
		.pipe(cssMin());
	
	merge(vendorStream, appStream)
	    .pipe(concat('app.css'))
	    .pipe(gulp.dest(prop.config.dest+'/assets/css'));
});

//Minify and move HTML and XML files to destination folder
gulp.task('templates',function(){
	gulp.src(['!'+prop.config.path+'/_temp/**',prop.config.path+'/**/*.{html,xml}'])
		.pipe(minifyHTML({
		    conditionals: true,
		    spare:true
		}))
		.pipe(gulp.dest(prop.config.dest));
});

//Minify and move json files to destination folder
gulp.task('json',function(){
	gulp.src(['!'+prop.config.path+'/_temp/**',prop.config.path+'/**/*.json'])
		.pipe(jsonMinify())
		.pipe(gulp.dest(prop.config.dest));
});

//Move files from temp folder to destination folder
gulp.task('temp',function(){
	gulp.src(prop.config.path+'/_temp/**/**')
		.pipe(gulp.dest(prop.config.dest));
});


// Minify core JS files and copy the result file and html components templates to destination path 
gulp.task('core',function(){
	gulp.start(['scripts', 'styles','templates','json','temp']);	
});

// Copy vendor fonts
gulp.task('fonts',function(){
	var fonts = prop.fonts.map(function(item) {
		return item+'/*.{ttf,woff,woff2,eot,svg}';
	});	
	
	gulp.src(fonts)
		.pipe(gulp.dest(prop.config.dest+'/assets/fonts'));
});

// Watch modifications in the files
gulp.task('watch', function () {
    watch([prop.config.path+'/**'], batch(function (events, done) {
        gulp.start('core', done);
    }));    
});

// Clean dest directory
gulp.task('clean', function(){
	return gulp.src(prop.config.dest, {read: false,force: true})
    	.pipe(clean());
});

// Optimize images
gulp.task('imagemin', function() {
    gulp.src(prop.config.path+'/assets/img/*.{jpg,jpeg,svg,gif,png,ico}' )
        .pipe(imagemin())
        .pipe(gulp.dest(prop.config.dest + '/assets/img'));
});

// Serve pages and live reload core changes
gulp.task('serve', ['watch'], function () {
	gulp.src(prop.config.dest)
		.pipe(server({
			livereload: {
				enable: true,
				// Configure livereload to ignore changes, only handle changes to the compiled CSS
//				filter: function (filename, cb) {
//					cb(!/\.(sa|le)ss$|node_modules/.test(filename));
//				}
			},
			port:prop.config.port,
			open: true,
			// HTML5 mode: non-hash URLs for my single page app
			fallback: 'index.html',
		    fallbackLogic: function(req, res, fallbackFile) {
		    	if ( req.url.match(/[.]+[\w]+$/i) ) {
		    		// request to a file with any extension
		    		res.statusCode = 404;
		            res.end();
		    	} else {
		            // default fallback config
		            fs.createReadStream(fallbackFile).pipe(res);
		        }
		    },
	    }));
});

gulp.task('build',['clean'], function () {
    gulp.start(['core', 'fonts', 'imagemin']);
});

gulp.task('default', ['build','watch','serve']);