(function() {
    'use strict';

	angular.module('app.filters')
	.filter('<%= camelCaseName %>', function () {
	    return function (text) {
	        return text;
	    }
	});
})();
