(function() {
    'use strict';
    
    angular.module('app').component('<%= camelCaseName %>', {
		templateUrl: './components/<%= name %>/<%= name %>.html',
		//transclude: true,
		bindings: {
			//lista: '='
		},
		controller: <%= camelCaseName %>Controller
	});
	
	/*@ngInject*/
	function <%= camelCaseName %>Controller() {
	    
	    var vm = this;
	
	    vm.$onInit = function() {
	    	vm.uniqueId = String(performance.now()).replace('.','');
	    };
	}
})();