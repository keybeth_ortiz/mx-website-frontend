(function() {
    'use strict';
	
	angular.module('app').controller('<%= camelCaseName %>Ctrl', <%= camelCaseName %>Ctrl);

	/*@ngInject*/
	function <%= camelCaseName %>Ctrl($rootScope, $state) {
		var vm = this;
		
		vm.$onInit = function(){
			
		};
	}
})();