(function() {
    'use strict';
	
	angular.module('app').factory('<%= camelCaseName %>Service', <%= camelCaseName %>Service);

	/*@ngInject*/
	function <%= camelCaseName %>Service($http,CONSTANTS) {
	    var service = {
	        get: get,
	        getAll: getAll,
	        post:post,
	        put:put,
	        delete:delete
	    };
	    return service;
	
	    function getAll() {
	        return $http.get(CONSTANTS.API_URL + '/<%= name %>/').then(function(resp) {
	            return resp.data;
	        });
	    }
	    
	    function get(id) {
	        return $http.get(CONSTANTS.API_URL + '/<%= name %>/',{params:{id:id}}).then(function(resp) {
	            return resp.data;
	        });
	    }
	    
	    function post(data) {
	        return $http.post(CONSTANTS.API_URL + '/<%= name %>/',data).then(function(resp) {
	            return resp.data;
	        });
	    }
	    
	    function put(id,data) {
	        return $http.put(CONSTANTS.API_URL + '/<%= name %>/'+id,data).then(function(resp) {
	            return resp.data;
	        });
	    }
	    
	    function delete(id) {
	        return $http.delete(CONSTANTS.API_URL + '/<%= name %>/'+id).then(function(resp) {
	            return resp.data;
	        });
	    }
	}
})();