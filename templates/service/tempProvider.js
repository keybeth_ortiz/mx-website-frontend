(function() {
    'use strict';
	
	angular.module('app').provider('<%= camelCaseName %>',  <%= camelCaseName %>Provider);

	/*@ngInject*/
	function <%= camelCaseName %>Provider() {
		var vm = this;
		
		vm.options = {};
		
		/*@ngInject*/
	    vm.$get = function (CONSTANTS) {
	    	
	    	function config(){};
	    	
	    	return {
	            config:config,
	            options:vm.options;
	    	};
	    };
	}
})();