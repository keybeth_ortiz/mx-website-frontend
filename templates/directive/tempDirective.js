(function() {
    'use strict';

	angular.module('app.directives')
	.directive('<%= camelCaseName %>', function(){
	    return {
	        restrict: 'EA',
	        link: function(scope, element, attrs, controller){
	            
	        }
	    };
	});
})();