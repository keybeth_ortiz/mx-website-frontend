(function() {
    'use strict';

	angular.module('app.directives')
	.directive('ngModel', function attributeNgModelDirective() {
	  return {
	    require: 'ngModel',
	    link: function(scope, el, attrs, ctrl) {
	      ctrl.$attributes = attrs;
	    }
	  };
	});
})();