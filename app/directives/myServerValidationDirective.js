(function() {
    'use strict';

	angular.module('app.directives')
	.directive('myServerValidation', function(){
	    return {
	        restrict: 'A',
	        require: 'form',
	        link: function(scope, element, attrs, formController){
	            scope.$watchCollection(function(){
	        		return formController;
	        	}, function(updatedFormController){
	                Object.getOwnPropertyNames(updatedFormController).forEach(function(field) {
	                    // Search for form controls with ng-model controllers
	                    // Which do not have attached server error resetter validator
	                    if (angular.isObject(updatedFormController[field]) 
	                    		&& updatedFormController[field].hasOwnProperty('$modelValue') 
	                    		&& angular.isObject(updatedFormController[field].$validators) 
	                    		&& !updatedFormController[field].$validators.hasOwnProperty('serverValidityResetter')) {                    	
	                        updatedFormController[field].$validators.serverValidityResetter = function(){
	                        	updatedFormController[field].$setValidity('serverError', true);
	                        	updatedFormController.$setValidity('serverError', true);
	                        	return true
	                        };
	                    }
	                });
	            });
	        }
	    };
	});
})();