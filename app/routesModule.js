(function() {
    'use strict';

	angular.module('app.routes', []).config(configure);

	/*@ngInject*/
	function configure($stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider) {
	    $urlRouterProvider.otherwise("/");

	    // Defines whether URLs should match trailing slashes, or not (the default behavior).
	    $urlMatcherFactoryProvider.strictMode(false);
	
	    // Requires server side rewrites url, as .htaccess in Apache
	    $locationProvider.html5Mode(true);
	    
	    $stateProvider
	        .state('app', {
	            url: '/',
	            views: {
	            	'@': {
	                    templateUrl: 'layouts/layout.html'
	                },
	                'contentView@app': {
	                	 templateUrl: 'pages/login/login.html',
	                     controller: 'loginCtrl as ctrl'
	                }
	            }
	        })
	        .state('app.dashboard', {
	            url: 'dashboard',
	            views: {
	                'contentView@app': {
	                    templateUrl: 'pages/dashboard/dashboard.html',
	                    controller: 'dashboardCtrl as ctrl'
	                }
	            },
	            data: {
	            	requiresLogin: true
	            }
	        });
	};

})();