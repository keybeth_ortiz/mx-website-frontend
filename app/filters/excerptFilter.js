(function() {
    'use strict';

	angular.module('app.filters')
	.filter('excerpt', function () {
	    return function (text, length) {
	        if (text.length > length) {
	            return text.substr(0, length) + '...';
	        }
	        return text;
	    }
	});
})();
