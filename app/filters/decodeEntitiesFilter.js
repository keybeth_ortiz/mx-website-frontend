(function() {
    'use strict';

	angular.module('app.filters')
	.filter('decodeEntities', function() {
		var element = document.createElement('div');
		return function(value) {		
			if(value && typeof value === 'string') {
	
	            // Escape HTML before decoding for HTML Entities
				value = escape(value).replace(/%26/g,'&').replace(/%23/g,'#').replace(/%3B/g,';');
	
	            element.innerHTML = value;
	            if(element.innerText){
	            	value = element.innerText;
	                element.innerText = '';
	            }else{
	                // Firefox support
	            	value = element.textContent;
	                element.textContent = '';
	            }
	        }
	        return unescape(value);
		}
	});
})();
