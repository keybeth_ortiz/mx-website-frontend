(function() {
    'use strict';

	angular.module('app',[ 
		'ui.router',
		'ui.bootstrap',
		'ui.utils.masks',
		'angular-jwt',
		'pascalprecht.translate',
		'tmh.dynamicLocale',
		'LocalStorageModule',
		'ngCookies',
		'ngMessages',
		'ngSanitize',
		'vcRecaptcha',
		'ngAnimate',
		'toaster',
		'blockUI',
		'app.directives',
		'app.filters',
		'app.routes'
	])
	.config(configApp)
	.run(runBlock);

	
	// Manual boostrapping angular 
	angular.element(document).ready(
		function() {
			var $http = angular.injector(['ng']).get('$http');
			$http.get('config.json').then(function (config) {
				return config.data;
		    },function (error) {
		        return { 
		        	API_URL: 'http://localhost:3000',
					VERSION: 'v1',
					DEBUG: true
				};
		    }).then(function(config) {
		    	$http.get(config.API_URL+'/api/properties').then(function(response) {
		    		config = angular.extend({}, config, response.data);
			        bootstrapApplication(config);
				},function(error){
					alert('We can\'t init without server properties');
				});
			});
		}
	);
	
	function bootstrapApplication(config) {
		angular.module('app').constant('CONSTANTS', config);
        angular.bootstrap(document, ['app']);
    }

	/*@ngInject*/
	function configApp($httpProvider, jwtOptionsProvider, $translateProvider,tmhDynamicLocaleProvider, localStorageServiceProvider,vcRecaptchaServiceProvider, CONSTANTS, $logProvider, blockUIConfig) {
		
		// --------------------------------------------------------------------------------- //
		// Config logger
		$logProvider.debugEnabled(CONSTANTS.DEBUG || false);
		
		// --------------------------------------------------------------------------------- //
		// Config recaptcha
		vcRecaptchaServiceProvider.setDefaults({
	    	key: CONSTANTS.RECAPTCHA_KEY,
	    	theme: 'light',
	    	size: 'invisible'
	  	});
	
		// --------------------------------------------------------------------------------- //
		// Config local storage, a prefix to differentiate our module
		localStorageServiceProvider.setPrefix('app');
	
		// --------------------------------------------------------------------------------- //
		// Config JWT to get token from Local Storage and redirect pages. The JWT token is
		// send to backend server in header Authorization, type Bearer. This parameters
		// also we can to configure here.
		jwtOptionsProvider.config({
			whiteListedDomains : [ 'localhost' ],
			// Support to UI-Router 1.0
			loginPath: '/',
			tokenGetter : [
				'options',
				'localStorageService',
				function(options, localStorageService) {
					// Skip authentication for any requests ending in .html
					if (options && options.url && options.url.substr(options.url.length - 5) == '.html') {
						return null;
					}
					return localStorageService.get('id_token');
				} 
			],
			authHeader: CONSTANTS.AUTHORIZATION_HEADER_NAME,
	        authPrefix: CONSTANTS.AUTHORIZATION_TOKEN_PREFIX
		});
	
		// --------------------------------------------------------------------------------- //
		// Configure http interceptors. 
		// - jwtInterceptor: put the token in the Authorization Header and validate response 
		//   status 401. 
		// - responseInterceptor: validate response from API.
		// - requestInterceptor: modify request before send to API. 
		$httpProvider.interceptors.push('jwtInterceptor');
		$httpProvider.interceptors.push('responseInterceptor');
		$httpProvider.interceptors.push('requestInterceptor');
		
		// --------------------------------------------------------------------------------- //
		// Set location to angular core translation files
		tmhDynamicLocaleProvider.localeLocationPattern('i18n/angular-locale_{{locale}}.js');
	
		// --------------------------------------------------------------------------------- //
		// Configure translate module to static files (We can configure an url too).
		// Include message formatter to allow plural to translation strings and scape html
		// entities from translation strings. The module determine default language at the 
		// beginning
		$translateProvider.useStaticFilesLoader({
			prefix : 'i18n/locale-',
			suffix : '.json'
		})
		.registerAvailableLanguageKeys([ 'en', 'es' ], {
			'en_*' : 'en',
			'es_*' : 'es'
		})
		.useCookieStorage()
		.useSanitizeValueStrategy('sanitizeParameters')
		.addInterpolation('$translateMessageFormatInterpolation')
		.determinePreferredLanguage();
	}
	
	/*@ngInject*/
	function runBlock($rootScope, $state, tmhDynamicLocale, authManager, authService, $transitions, localStorageService, jwtHelper, $log, notificationService, translateFilter, blockUIConfig) {
		$rootScope.date = new Date();
		$rootScope.toasterOptions = {
			'time-out': 10000,
			'close-button': true
		};
		$rootScope.hasAnyRole = function(roles) {
			return authService.hasAnyRole(roles);
		};
		$rootScope.hasRole = function(role) {
			return authService.hasRole(role);
		};
		$rootScope.$on('authenticated', function(response) {
			$log.debug('Authenticated');
        	$state.go('app.dashboard');
		});
		$rootScope.$on('unauthorized', function(response) {
			// Server send 403 status
			$log.debug('Unauthorized');
			notificationService.warning('error.unauthorized');
			// Only in this case when we don't have a page without permissions
			authService.logout();
		});
	
		// --------------------------------------------------------------------------------- //
		// Configure JWT Module to redirect when unauthenticated and check when refresh 
		// the page. Then, the events broadcasted for JWT Module
		authManager.redirectWhenUnauthenticated();

		// --------------------------------------------------------------------------------- //
		// Mandatory after authManager check when refresh
		authService.checkAuthOnRefresh();
		
		$rootScope.$on('tokenHasExpired', function() {
			// logout or use refresh token
			$log.debug('Token Has Expired');
			notificationService.warning('error.token.expired');
			authService.logout();
		});
		
		$rootScope.$on('unauthenticated', function(response) {
			// Server send 401 status or logout
			$log.debug('Logout');
			$rootScope.token = null;
	    	localStorageService.clearAll();
		});
	
		// --------------------------------------------------------------------------------- //
		// Listeners languages changes events, only 1 active.
		$rootScope.$on('$translateChangeSuccess', function(event,data) {
			$log.debug('Change language:',data.language);
			$rootScope.lang = data.language;
			tmhDynamicLocale.set(data.language);
			// Change language message to Block IU (Also we could change the template in config block)
			blockUIConfig.message = translateFilter('loading');
		});
	
		//$rootScope.$on('$localeChangeSuccess', function(event,data) {
		//	$rootScope.lang = data;
		//	$translate.use(data);
		//});
		
		// --------------------------------------------------------------------------------- //
		// Listeners states changes events. Mandatory after authService check when refresh
		$transitions.onStart( {}, function(trans) {
			var toState = trans.$to();
			$log.debug('Request change state to:',toState.name);
			
			// Based in states authorization roles check if the user is authorized to view the states
			if (toState.data && toState.data.requiresRole) {
				if (! authService.hasAnyRole(toState.data.requiresRole) ) {
					$rootScope.$broadcast('unauthorized');
					return false;
				}
			}
		});
	
		$transitions.onError( {}, function(trans) {
			var error = trans.error();
			// Oculting Error: transition superseded (Redirect a state)
			// https://github.com/angular-ui/ui-router/issues/3246#issuecomment-287513409
			$log.debug('State error:',error);
		});
	
		$transitions.onSuccess( {}, function(trans) {
			$log.debug('Change state to:',trans.$to().name);
			$rootScope.state = trans.$to().name;
		});
	
		// --------------------------------------------------------------------------------- //
		// Listeners to local storage events.
		$rootScope.$on('LocalStorageModule.notification.setitem',function(item) {
			//TODO anything
		});
	
		$rootScope.$on('LocalStorageModule.notification.removeitem',function(item) {
			//TODO anything
		});
	}

})();