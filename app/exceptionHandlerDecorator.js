(function() {
    'use strict';

	angular.module('app')
	.decorator('$exceptionHandler', extendExceptionHandler);
	
	/*@ngInject*/
	function extendExceptionHandler($delegate, $log){
		return function(exception, cause) {
	        $log.debug('Default Exception Handler');
	        //TODO Anything, for example use Raven to save in Sentry
	        //var $rootScope = $injector.get("$rootScope");
	        $delegate(exception, cause);
	    };
	}
})();