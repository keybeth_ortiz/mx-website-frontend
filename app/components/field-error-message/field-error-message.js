(function() {
    'use strict';

    angular.module('app').component('fieldErrorMessage', {
		templateUrl: './components/field-error-message/field-error-message.html',
		transclude: true,
		bindings: {
			field: '<'
		},
		controller: fieldErrorMessageController
	});
	
	/*@ngInject*/
	function fieldErrorMessageController($scope,translateFilter, $rootScope) {
	    
	    var vm = this;
	
	    vm.$onInit = function() {
	    	vm.uniqueId = String(performance.now()).replace('.','');
	    	vm.field = vm.field || {};
	    	setScope();
	    };
	    
	    vm.$onChanges = function(changes) {
	    	//TODO Don't work $onChanges when modify $attributes
	    	if(changes.$attributes)
	    		setScope();
	    };
	    
	    $rootScope.$on('$translateChangeSuccess', function(event,data) {
	    	$scope.attribute = translateFilter('attribute.'+vm.field.$attributes.name);
	    });
	    
	    function setScope(){
	    	Object.keys(vm.field.$attributes).forEach(function (key) {
	    		$scope[key]= vm.field.$attributes[key];
			});
	    	$scope.attribute = translateFilter('attribute.'+vm.field.$attributes.name);
	    }
	}
})();