(function() {
    'use strict';

	angular.module('app').component('navbar', {
		templateUrl: './components/navbar/navbar.html',
		bindings: {
			//lista: '='
		},
		controller: navbarController
	});
	
	/*@ngInject*/
	function navbarController(authService, $rootScope) {
	    
	    var vm = this;
	    vm.$rootScope = $rootScope;
	    vm.isCollapse = true;
	
	    vm.$onInit = function() {
	    	vm.uniqueId = String(performance.now()).replace('.','');
	    };
		
		vm.onLogout = function() {
			authService.logout();
		};
	 }
})();