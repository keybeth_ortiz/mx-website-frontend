(function() {
    'use strict';
    
    angular.module('app').component('languageDropdown', {
		templateUrl: './components/language-dropdown/language-dropdown.html',
		controller: languageDropdownController
	});
	
	/*@ngInject*/
	function languageDropdownController($translate) {
	    
	    var vm = this;
	
	    vm.$onInit = function() {
	    	vm.uniqueId = String(performance.now()).replace('.','');
	    };
		
		vm.changeLanguage = function(language) {
			$translate.use(language);
		};
	}
})();