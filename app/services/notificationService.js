(function() {
    'use strict';

	angular.module('app').factory('notificationService', notificationService);

	/*@ngInject*/
	function notificationService(translateFilter, toaster) {
	    var service = {
	        info: info,
	        error: error,
	        warning:warning,
	        success:success
	    };
	    return service;
	
	    function info(message) {
	    	toaster.pop('info', translateFilter('notification.info'),translateFilter(message));
	    }
	    
	    function error(message) {
	    	toaster.pop('error', translateFilter('notification.error'),translateFilter(message));
	    }
	    
	    function warning(message) {
	    	toaster.pop('warning', translateFilter('notification.warning'),translateFilter(message));
	    }
	    
	    function success(message) {
	    	toaster.pop('success', translateFilter('notification.success'),translateFilter(message));
	    }
	}
})();