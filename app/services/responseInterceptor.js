(function() {
    'use strict';

	angular.module('app').factory('responseInterceptor', responseInterceptor);

	/*@ngInject*/
	function responseInterceptor($q, notificationService, CONSTANTS, localStorageService, $rootScope) {
	    return {
		response:response,
	        responseError: responseError
	    };
	    
	    function response(response){
	    	var authHeader = response.headers(CONSTANTS.AUTHORIZATION_HEADER_NAME);
	    	if(authHeader){
	    		var token = authHeader.replace(CONSTANTS.AUTHORIZATION_TOKEN_PREFIX,"");
        		localStorageService.set('id_token',token);
	    	}
	        return response || $q.when(response);
	    }
	    
	    function responseError(response){
	    	//TODO Manage global http errors
	    	if(response.status < 0){
	    		// Error connecting with the server
	    		notificationService.error('error.http.connection');
	    	}else if(response.status == 0){
	    		// Error 0, problem with CORS
	    		notificationService.error('error.http.cors');
	    	}else if(response.status == 403){
	    		//Unauthorized
	    		$rootScope.$broadcast('unauthorized');
	    	}else if(response.data && response.data.message){
	    		//TODO response.data.message or response.data.code or we can have our own messages for error codes from backends
	    		notificationService.error(response.data.message);
	    	}else{
	    		notificationService.error('error.http.unknown');
	    	}
	    	return $q.reject(response);
	    }    
	};
})();
