(function() {
    'use strict';
	
	angular.module('app').factory('exchangeRateService', exchangeRateService);

	/*@ngInject*/
	function exchangeRateService($http,CONSTANTS) {
	    var service = {
	        lastest: lastest,
	    };
	    return service;
	
	    function lastest(data) {
	    	//return $http.get('http://api.fixer.io/latest',{params:data}).then(function(resp) {
	        return $http.get(CONSTANTS.API_URL + '/api/rate/lastest',{params:data}).then(function(resp) {
	            return resp.data;
	        });
	    }
	}
})();