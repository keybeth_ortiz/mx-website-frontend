(function() {
    'use strict';

	angular.module('app').factory('requestInterceptor', requestInterceptor);

	/*@ngInject*/
	function requestInterceptor($q, $translate) {
	
		return {
	        request: request
	    };
	    
	    function request(config) {
	    	config.headers['Accept-Language'] = $translate.use();	        
	        return config;
	    }
	    
	    function requestError(rejectReason) {            	
	    	if (rejectReason === 'reason') {
	    		// TODO anything
	    		return null;
	    	}
	        return $q.reject(rejectReason);
	    }
	};
})();
