(function() {
    'use strict';

	angular.module('app').factory('authService', authService);

	/*@ngInject*/
	function authService($rootScope, jwtHelper, authManager, localStorageService, $state, $http,$injector, CONSTANTS) {
	    var service = {
	        login: login,
	        logout: logout,
	        hasRole: hasRole,
	        hasAnyRole: hasAnyRole,
	        checkAuthOnRefresh: checkAuthOnRefresh
	    };
	    return service;
	    
	    function login(credentials){
	    	return $http.post(CONSTANTS.API_URL+'/login',credentials, {skipAuthorization: true}).then(function(response) {
	    		$rootScope.$broadcast('authenticated', response.data);
			});
	    }
	    
	    function logout(){
	    	// Send same event that jwt Module
	    	$rootScope.$broadcast('unauthenticated');
	    }
	    
	    function hasRole(role){
	    	if( ! $rootScope.isAuthenticated ){
	    		return false;
	    	}
	    	var token = $rootScope.token;
	    	if ( !token || ! token.roles ) {
	            return false;    		
	        }
	    	return token.roles.indexOf(role) !== -1;
	    }
	  
	    function hasAnyRole(roles){
	    	if( ! $rootScope.isAuthenticated ){
	    		return false;
	    	}
	    	var token = $rootScope.token;
	    	if ( !token || ! token.roles ) {
	            return false;    		
	        }
	    	if(Array.isArray(roles)){
	    		for(var i in roles){
	    			if (token.roles.indexOf(roles[i]) !== -1){
	    				return true;
	    			}
				}
	    	}else if (typeof roles === 'string' || roles instanceof String){
	    		return token.roles.indexOf(roles) !== -1;
	    	}
	    	return false;
	    }
	    
	    function checkAuthOnRefresh(){
	    	authManager.checkAuthOnRefresh();
	    	var $transitions = $injector.get('$transitions');
	    	$transitions.onStart({}, function(){
	    		if( $rootScope.isAuthenticated ){
	    			$rootScope.token = jwtHelper.decodeToken(authManager.getToken());
		    	}
	    	});
	    }
	    
	}
})();