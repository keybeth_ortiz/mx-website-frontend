(function() {
    'use strict';

	angular.module('app').factory('formHelperService', formHelperService);

	/*@ngInject*/
	function formHelperService($q) {
	    var service = {
	    	responseServerErrors: responseServerErrors
	    };
	    return service;
	    
	    var TRANSLATE_BACKEND_ERROR = {
				Minlength: 	'minlength',
				Maxlength: 	'maxlength',
				NotEmpty: 	'required',
				NotBlank: 	'required',
				NotNull: 	'required',
				Pattern: 	'pattern',
				Email: 		'email',
				Max: 		'max',
				Min: 		'min'
		};
	
	    function responseServerErrors(errorResponse, formController) {
			if(errorResponse.status == 422){
				errorResponse.data.errors.forEach(function(item) {
					if( item.field 
							&& angular.isObject(formController[item.field]) 
							&& formController[item.field].hasOwnProperty('$modelValue')){
						var type = TRANSLATE_BACKEND_ERROR[item.type] || item.type;
						if( type && formController[item.field].$validators.hasOwnProperty(type)){
							formController[item.field].$setValidity(type, false);
						}else{
							formController[item.field].$setValidity('serverError', false);
							//TODO It should throw field-error-message $onChanges method
							formController[item.field].$attributes.serverError = item; 
						}
						formController[item.field].$setTouched();
					}else{
						formController.$setValidity('serverError', false);
						formController.$setSubmitted();
					}
				});					
			}
			return $q.reject(errorResponse);
		}
	}
})();