(function() {
    'use strict';

	angular.module('app').controller('loginCtrl', loginCtrl);

	/*@ngInject*/
	function loginCtrl($rootScope, $state,authService, vcRecaptchaService, formHelperService) {
		var vm = this;
		vm.login = {};
		vm.loginForm;
		vm.recaptchaWidgetId;
		
		vm.$onInit = function(){
	
		};
		
		vm.onLogin = function() {
			if( !vm.disableSubmit())
				vcRecaptchaService.execute(vm.recaptchaWidgetId);
		};
		
		vm.responseCaptcha = function(response) {
			// The ngModel failed
			vm.login.recaptcha = response;
			login();
		};
		
		vm.setWidgetId = function(widgetId){
			vm.recaptchaWidgetId = widgetId;
		};
		
		vm.resetCaptcha = function(){
			vcRecaptchaService.reload(vm.recaptchaWidgetId);
		};
		
		vm.disableSubmit = function() {
			var errors = Object.keys(vm.loginForm.$error);
			return vm.loginForm.$invalid && (errors.length != 1 || errors.indexOf('recaptcha') == -1);
		};
	
		$rootScope.$on('$translateChangeSuccess', function(event,data) {
			if(vm.recaptchaWidgetId !== undefined)
				vcRecaptchaService.useLang(vm.recaptchaWidgetId,data.language);
		});
		
		function login() {
			if(vm.loginForm.$valid){
				authService.login(vm.login)
				.catch(function(errorResponse) {
					vm.resetCaptcha();
					formHelperService.responseServerErrors(errorResponse,vm.loginForm);
				});
			}else{
				vm.resetCaptcha();
			}
		}
	}
})();