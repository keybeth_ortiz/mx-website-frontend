(function() {
    'use strict';

	angular.module('app').controller('dashboardCtrl', dashboardCtrl);

	/*@ngInject*/
	function dashboardCtrl(formHelperService, exchangeRateService, $scope,$interval) {
		var vm = this;
		vm.quoteForm;
		vm.quote = {
			base:  'USD',
			symbols:'EUR'
		};
		vm.rates;
		vm.refresh;
		
		vm.$onInit = function(){
			setRatesData();
			vm.refresh = $interval(setRatesData, 10*60*1000); //10 min			
		};
		
		vm.onQuote = function () {
			if(vm.quoteForm.$valid){
				exchangeRateService.lastest(vm.quote)
				.then(function(response) {
					if(response && response.rates && response.rates[vm.quote.symbols])
						vm.quote.toAmount = vm.quote.baseAmount * response.rates[vm.quote.symbols];
					else
						vm.quote.toAmount = null;
				})
				.catch(function(errorResponse) {
					vm.quote.toAmount = null;
					formHelperService.responseServerErrors(errorResponse,vm.quoteForm);
				});
			}
		};

       $scope.$on('$destroy', function() {
        	stopRefresh();
        });
        
       function stopRefresh() {
			if (angular.isDefined(vm.refresh)) {
				$interval.cancel(vm.refresh);
				vm.refresh = undefined;
			}
       }
       
        function setRatesData(){
        	exchangeRateService.lastest({base: vm.quote.base})
			.then(function(response) {
				vm.rates = response.rates;
			});
        };
	
	}
})();
