# Moneyxchange.io

> Frontend para el website de Moneyxchange

---

Se requiere tener instalado [Node.js](https://nodejs.org)

Para instalar las dependencias de Node.js

    npm install

### Para producci�n

    gulp build
   
luego copiar todo el contenido de la carpeta **/dist** en el servidor donde correr� nuestra aplicaci�n.
 
### Para desarrollo

* Para generar el codigo con un mapa de los archivos originales para poder hacer debug en el explorador

    gulp build --env=dev

* Para hacer funcionar nuestra aplicaci�n en el servidor de node.js local. 
   
    gulp serve --env=dev

* Para construir nuevos componentes, servicios, p�ginas u otros de nuestra aplicaci�n

    gulp component --name=MyComponent
    gulp page --name=MyPage
    gulp service --name=MyService
    gulp provider --name=MyProvider
    gulp filter --name=MyFilter
    gulp directive --name=MyDirective
