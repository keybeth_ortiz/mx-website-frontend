// Karma configuration
// Generated on Thu Feb 22 2018 21:43:03 GMT-0300 (Hora estándar de Argentina)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
    	'./node_modules/angular/angular.min.js',
		'./node_modules/@uirouter/angularjs/release/angular-ui-router.min.js',
		'./node_modules/angular-jwt/dist/angular-jwt.js',
		'./node_modules/angular-dynamic-locale/dist/tmhDynamicLocale.min.js',
		'./node_modules/angular-translate/dist/angular-translate.min.js',
		'./node_modules/angular-translate/dist/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
		'./node_modules/messageformat/messageformat.min.js',
		'./node_modules/angular-translate/dist/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.min.js',
		'./node_modules/angular-translate/dist/angular-translate-storage-cookie/angular-translate-storage-cookie.min.js',
		'./node_modules/angular-local-storage/dist/angular-local-storage.min.js',
		'./node_modules/angular-cookies/angular-cookies.min.js',
		'./node_modules/angular-messages/angular-messages.min.js',
		'./node_modules/angular-recaptcha/release/angular-recaptcha.min.js',
		'./node_modules/angular-sanitize/angular-sanitize.min.js',
		'./node_modules/angular-animate/angular-animate.min.js',
		'./node_modules/angularjs-toaster/toaster.min.js',
		'./node_modules/angular-block-ui/dist/angular-block-ui.min.js',
		'./node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
		'./node_modules/angular-input-masks/releases/angular-input-masks-standalone.min.js',
		'./node_modules/angular-mocks/angular-mocks.js',
		'./node_modules/karma-read-json/karma-read-json.js',
		'./**/*Module.js',
		'./app/**/*.js',
		{pattern: './test/**/*.json', included: false},
	    './test/**/*.js'
    ],


    // list of files / patterns to exclude
    exclude: [
      './app/_temp/**/*.js'
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
